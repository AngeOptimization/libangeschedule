#ifndef ANGE_SCHEDULE_TWINLIFTRULES_H
#define ANGE_SCHEDULE_TWINLIFTRULES_H

#include <QString>
#include <QMetaType>

namespace ange {
namespace schedule {

struct TwinLiftRules {

    /**
     * enum describing the possible twinlift rules.
     */
    enum Types {
        TwinliftRuleUnknown = 0,
        TwinliftRuleAll = 1,
        TwinliftRuleNothing = 2,
        TwinliftRuleBelow = 3
    };

    static QString toString(Types rule) {
        switch (rule) {
            case TwinliftRuleUnknown :
                return QString("");
            case TwinliftRuleAll :
                return QString("yes");
            case TwinliftRuleNothing :
                return QString("no");
            case TwinliftRuleBelow :
                return QString("below");
        };
        Q_ASSERT(false);
        return QString("");
    }

};

}}  // namespace ange::schedule

Q_DECLARE_METATYPE(ange::schedule::TwinLiftRules::Types)

#endif  // ANGE_SCHEDULE_TWINLIFTRULES_H
