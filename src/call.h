#ifndef LIBANGESCHEDULE_CALL_H
#define LIBANGESCHEDULE_CALL_H

#include "angeschedule_export.h"

#include "cranerules.h"
#include "twinliftrules.h"

#include <ange/utils/identifiable.h>

#include <QDateTime>
#include <QString>
#include <QSharedPointer>
#include <QDebug>
#include <QObject>
#include <QMetaType>

namespace ange {
namespace schedule {

/**
 * A single call in a schedule
 */
class ANGESCHEDULE_EXPORT Call : public QObject, public ange::utils::Identifiable<Call> {

    Q_OBJECT

public:

    /**
     * Simple constructor
     * @param uncode UN/LOCODE
     * @param voyage_code voyage code for this call
     * @param parent in qobject hiearchy
     * @param id unique id for this call in this application
     */
    Call(const QString& uncode, const QString& voyage_code, QObject* parent = 0, int id = -1);

    /**
     * Simple constructor
     * @param uncode UN/LOCODE
     * @param voyage_code voyage code for this call
     * @param port_name Name of port, default is the uncode
     * @param eta estimated time of arrival
     * @param etd estimated time of departure
     * @param parent in qobject hiearchy
     * @param id unique id for this call in this application
     */
    Call(const QString& uncode, const QString& voyage_code, const QString& port_name, const QDateTime& eta,
         const QDateTime& etd, QObject* parent = 0, int id = -1);

    ~Call();

    /**
     * Set the number of cranes available in call
     * This emits cranes_changed signal if cranes change
     */
    void setCranes(double cranes);

    /**
     * Set the max number of HC containers to stow in overdeck stacks according to the capabilities of
     * the cranes in the call.
     * This emits height_limit_changed signal if the number changes
     */
    void setHeightLimit(double height_limit);

    /**
     * Set the crane bin rule for the call. This can be that the cranes need bins of at least 40 foot or 80 foot.
     */
    void setCraneRule(CraneRules::Types rule);

    /**
     * @return set voyage code
     * This emit voyage_code_changed if changed
     */
    void setVoyageCode(QString voyage_code);

    /**
     * Sets the twinlifting status, true if the call support twinlifting
     * This emits call_changed() if changed
     */
    void setTwinlifting(TwinLiftRules::Types twinlifting);

    /**
     * Sets the ETA for call.
     */
    void setEta(QDateTime eta);

    /**
     * Sets the ETD for call.
     */
    void setEtd(QDateTime etd);

    /**
     * Sets the productivity
     */
    void setCraneProductivity(double productivity);

    /**
     * @return the UN/LOCODE
     */
    const QString& uncode() const;

    /**
     * @return the name
     */
    const QString& name() const;

    /**
     * @return expected time of arrival
     */
    const QDateTime& eta() const;

    /**
     * @return expected time of departure
     */
    const QDateTime& etd() const;

    /**
     * @return associated voyage code
     */
    const QString& voyageCode() const;

    /**
     * @return the number of full-time cranes available at at call. Part-time cranes are converted to full-time
     * cranes as fractional numbers. Nan represents an unknown number of cranes.
     */
    double cranes() const;

    /**
     * @return the max number of HC containers to stow in stacks overdeck according to crane limitations.
     * Nan represents an unknown height limit.
     */
    double heightLimit() const;

    /**
     * @return the crane rule for the call. The rules are given in crane_rules_t. If a crane rule states forty foot bins, this means
     * that the minimal distance between working cranes is 40 foot.
     */
    CraneRules::Types craneRule() const;

    /**
     * @return the twinlifting rule for this call.
     */
    TwinLiftRules::Types twinLifting() const;

    /**
     * @return distance (in calls) between @param dest and current call. If dest and this does not belong to the same schedule,
     * the result is undefined.
     *
     * negative if dest is before this call in the schedule. @param dest must be a valid not-null call.
     */
    int distance(const Call* dest) const;

    /**
     * Compares two calls in the same schedule, the call first is the schedule is the smallest.
     * If the calls are not from the same schedule the result is undefined.
     */
    // Perhaps we should deprecated this and point to operator<() ?
    bool lessThan(const Call* other) const;

    /**
     * @return the average productivity per crane
     */
    double craneProductivity() const;

    /**
     * @return format uncode and voyage code to a user presentable string.
     */
    QString assembledName() const;

    /**
     * Pointer comparison as calls can't be copied
     */
    bool operator==(const Call& rhs) const {
        return this == &rhs;  // pointer comparison
    }

    bool operator!=(const Call& rhs) const {
        return !(*this == rhs);  // operator==
    }

    /**
     * Compares two calls in the same schedule, the call first is the schedule is the smallest.
     * If the calls are not from the same schedule the result is undefined.
     */
    bool operator<(const Call& rhs) const;

    bool operator<=(const Call& rhs) const {
        return *this == rhs || *this < rhs;  // operator== and operator<
    }

    bool operator>(const Call& rhs) const {
        return !(*this <= rhs);  // operator<=
    }

    bool operator>=(const Call& rhs) const {
        return !(*this < rhs);  // operator<
    }

Q_SIGNALS:

    /**
     * number of cranes has changed for this call.
     * @param cranes new number of cranes
     */
    void cranesChanged(double cranes);

    /**
     * height limit in HC for stacks overdeck have changed for this call.
     * @param cranes new number of cranes
     */
    void heightLimitChanged(double cranes);

    /**
     * voyage code has changed for this call
     * @param voyage_code new voyage code
     */
    void voyageCodeChanged(QString voyage_code);

    /**
     * crane rule has changed for this call
     * @param crane_rules_t new crane rule
     */
    void craneRuleChanged(CraneRules::Types rule);

    /**
     * twinlifting status has changed for this call
     */
    void twinLiftingChanged(TwinLiftRules::Types twinlifting);

    /**
     * ETA changed for this call
     */
    void etaChanged(QDateTime eta);

    /**
     * ETD changed for this call
     */
    void etdChanged(QDateTime etd);

    /**
     * productivity changed
     */
    void craneProductivityChanged(double newProductivity);

private:
    /**
     * Set by schedule
     */
    void setRotationNo(int rotation_no) const;

    int rotationNo() const;

    /**
     * Compares two calls in the same schedule, the call first is the schedule is the smallest.
     * If the calls are not from the same schedule the result is undefined.
     */
    bool privateLessThan(const Call* other) const;

private:
    class CallData;
    QScopedPointer<CallData> d;

private:
    Q_DISABLE_COPY(Call)
    friend class Schedule;
    friend class SchedulePrivate;

};

ANGESCHEDULE_EXPORT QDebug operator<<(QDebug str, const Call& call);
ANGESCHEDULE_EXPORT QDebug operator<<(QDebug str, const Call* call);

} // namespace schedule
} // namespace ange

Q_DECLARE_METATYPE(const ange::schedule::Call*)

#endif
