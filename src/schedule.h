#ifndef LIBANGESCHEDULE_SCHEDULE_H
#define LIBANGESCHEDULE_SCHEDULE_H

#include "angeschedule_export.h"
#include <QList>
#include <QScopedPointer>
#include <QObject>

namespace ange {
namespace schedule {

class Call;
class SchedulePrivate;

/**
 * Schedule is an ordered list of calls
 */
class ANGESCHEDULE_EXPORT Schedule : public QObject {

    Q_OBJECT

public:

    /**
     * Construct schedule_t with calls, the schedule_t will call delete on the calls when destroyed
     * @param calls
     *   The calls in the schedule
     */
    Schedule(QList< ange::schedule::Call* > calls, QObject* parent = 0) ;

    ~Schedule() ;

    /**
     * Get list of all calls in schedule
     */
    const QList<Call*>& calls() const;

    /**
     * Number of calls in schedule
     */
    int size() const;

    /**
     * @returns the first call after (not including) the given call that has the given uncode
     */
    const Call* getCallByUncodeAfterCall(const QString& uncode, const ange::schedule::Call* from) const;

    /**
     * @returns the last call before or at the given call that has the given uncode
     */
    const Call* getCallByUncodeBeforeCall(const QString& uncode, const ange::schedule::Call* before) const;

    /**
     * @returns the Call with a given ID. If not found, a nullpointer is returned.
     */
    const Call* getCallById(int id) const;

    /**
     * @returns the Call with a given ID. If not found, a nullpointer is returned. Non-const version.
     */
    Call* getCallById(int id);

    /**
     * @return true iff lhs is earlier in the schedule than rhs. If either lhs or rhs is
     * missing from schedule, the behaviour is undefined
     */
    bool less(const Call* lhs, const Call* rhs) const;

    /**
     * inserts a call in the given position. Position has to be <= calls.size()
     */
    void insert(int position, ange::schedule::Call* call);

    /**
     * removes a call at a given position. Position has to be less than calls.size();
     */
    void removeAt(int position);

    /**
     * moves port at @param position1 to @param position2 in the list where the port has been removed from position1
     *
     * WARNING! that the way position2 is handled is bad because it not like QAbstractItemModel::beginMoveRows()
     */
    void changeOfRotation(int position1, int position2);

    /**
     * @returns i'th call in schedule
     */
    Call* at(int i) const;

    /**
     * @return index of call, or -1 if not found
     */
    int indexOf(const ange::schedule::Call* call)  const;

    /**
     * @return list of calls between two calls, from inclusive, to exclusive
     */
    QList<Call*> between(const Call* from, const Call* to) const;

    /**
     * @return the following call in the schedule (or null if last call)
     */
    const Call* next(const ange::schedule::Call* call) const;

    /**
     * @return the following call in the schedule (or null if last call)
     */
    Call* next(const ange::schedule::Call* call);

    /**
     * @return the previous call in the schedule (or null if  first call);
     */
    const ange::schedule::Call* previous(const ange::schedule::Call* call) const;

    /**
     * @return the previous call in the schedule (or null if  first call);
     */
    ange::schedule::Call* previous(const ange::schedule::Call* call);

    /**
     * @return the pivot port, typically the last port in the region
     */
    const ange::schedule::Call* pivotCall() const;

    /**
     * Set the pivot port, typically the last port in the region
     */
    void setPivotCall(const int pivot_port_index);

Q_SIGNALS:

    /**
     * this signal is emitted whenever the schedule is changed (a port is added,
     * removed or the port rotation is changed)
     */
    void scheduleChanged();

    /**
     * emitted just before a call is added
     */
    void callAboutToBeAdded(int position, ange::schedule::Call* call);

    /**
     * emitted after a call is added to the schedule
     */
    void callAdded(ange::schedule::Call* call);

    /**
     * emitted just before a call is removed
     */
    void callAboutToBeRemoved(ange::schedule::Call* call);

    /**
     * emitted after a call is removed to the schedule
     */
    void callRemoved(ange::schedule::Call* call);

    /**
     * Emitted right before a change of rotation. Will move a single call.
     * @param fromPosition position of call before move
     * @param toPosition new position of call after it has been removed from fromPosition
     *
     * WARNING! that the way toPosition is handled is bad because it not like QAbstractItemModel::beginMoveRows()
     */
    void rotationAboutToBeChanged(int fromPosition, int toPosition);

    /**
     * Emitted after a change of rotation
     * @param fromPosition position of call before move
     * @param toPosition new position of call after it has been removed from fromPosition
     *
     * WARNING! that the way toPosition is handled is bad because it not like QAbstractItemModel::beginMoveRows()
     */
    void rotationChanged(int fromPosition, int toPosition);

private:
    QScopedPointer<SchedulePrivate> d;

private:
    Q_DISABLE_COPY(Schedule)

};

} // namespace schedule
} // namespace ange

#endif
