#include <QTest>
#include <qsignalspy.h>
#include <QObject>
#include "../schedule.h"
#include "../call.h"

using ange::schedule::Schedule;
using ange::schedule::Call;

class TestCall : public QObject {
    Q_OBJECT

private Q_SLOTS:
    void testLessThan();
    void testComparisonOperators();
    void testAssembledName();

private:
    static Schedule* createSchedule();
};

Schedule* TestCall::createSchedule() {
    QList<Call*> calls;
    calls << new Call("AAAAA", "1");
    calls << new Call("BBBBB", "1");
    calls << new Call("CCCCC", "1");
    calls << new Call("DDDDD", "1");
    calls << new Call("EEEEE", "1");
    calls << new Call("FFFFF", "1");
    Schedule* schedule = new Schedule(calls);
    return schedule;
}

void TestCall::testLessThan() {
    Schedule* schedule = createSchedule();

    const Call* call0 = schedule->at(0);
    const Call* call1 = schedule->at(1);
    const Call* call2 = schedule->at(2);

    QCOMPARE(call1->lessThan(call0), false);
    QCOMPARE(call1->lessThan(call1), false);
    QCOMPARE(call1->lessThan(call2), true);

    QCOMPARE(call0->lessThan(call1), true);
    QCOMPARE(call1->lessThan(call1), false);
    QCOMPARE(call2->lessThan(call1), false);

    delete schedule;
}

void TestCall::testComparisonOperators() {
    Schedule* schedule = createSchedule();

    const Call* call0 = schedule->at(0);
    const Call* call1 = schedule->at(1);

    QCOMPARE(*call0 == *call1, false);
    QCOMPARE(*call1 == *call1, true);
    QCOMPARE(*call1 == *call0, false);

    QCOMPARE(*call0 != *call1, true);
    QCOMPARE(*call1 != *call1, false);
    QCOMPARE(*call1 != *call0, true);

    QCOMPARE(*call0 < *call1, true);
    QCOMPARE(*call1 < *call1, false);
    QCOMPARE(*call1 < *call0, false);

    QCOMPARE(*call0 <= *call1, true);
    QCOMPARE(*call1 <= *call1, true);
    QCOMPARE(*call1 <= *call0, false);

    QCOMPARE(*call0 > *call1, false);
    QCOMPARE(*call1 > *call1, false);
    QCOMPARE(*call1 > *call0, true);

    QCOMPARE(*call0 >= *call1, false);
    QCOMPARE(*call1 >= *call1, true);
    QCOMPARE(*call1 >= *call0, true);

    delete schedule;
}

void TestCall::testAssembledName() {
    Schedule* schedule = createSchedule();

    QCOMPARE(schedule->at(0)->assembledName(), QStringLiteral("AAAAA/1"));
    QCOMPARE(schedule->at(1)->assembledName(), QStringLiteral("BBBBB/1"));
    QCOMPARE(schedule->at(2)->assembledName(), QStringLiteral("CCCCC/1"));
    QCOMPARE(schedule->at(3)->assembledName(), QStringLiteral("DDDDD/1"));
    QCOMPARE(schedule->at(4)->assembledName(), QStringLiteral("EEEEE/1"));
    QCOMPARE(schedule->at(5)->assembledName(), QStringLiteral("FFFFF/1"));

    delete schedule;
}

QTEST_MAIN(TestCall);

#include "testcall.moc"
