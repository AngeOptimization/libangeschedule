#include <QTest>
#include <qsignalspy.h>
#include <QObject>
#include "../schedule.h"
#include "../call.h"

using ange::schedule::Schedule;
using ange::schedule::Call;


class TestChangeRotation : public QObject {
    Q_OBJECT
private:
    /**
     * Ownership of schedule is responsibility of caller
     */
    static Schedule* createSchedule();

private Q_SLOTS:
    void testMoveUp();
    void testMoveDown();
    void testMoveSame();
    void testMoveTwoDown();
    void testMoveTwoUp();
};

Schedule* TestChangeRotation::createSchedule() {
    QList<Call*> calls;


    calls << new Call("AAAAA", "1");
    calls << new Call("BBBBB", "1");
    calls << new Call("CCCCC", "1");
    calls << new Call("DDDDD", "1");
    calls << new Call("EEEEE", "1");
    calls << new Call("FFFFF", "1");
    Schedule* schedule = new Schedule(calls);

    return schedule;
}

void TestChangeRotation::testMoveDown() {
    Schedule* schedule = createSchedule();
    QSignalSpy spyabout(schedule, SIGNAL(rotationAboutToBeChanged(int, int)));
    QSignalSpy spydone(schedule, SIGNAL(rotationChanged(int, int)));
    QVERIFY(spyabout.isValid());
    QVERIFY(spydone.isValid());

    schedule->changeOfRotation(2, 3);

    QCOMPARE(schedule->at(0)->uncode(), QString("AAAAA"));
    QCOMPARE(schedule->at(1)->uncode(), QString("BBBBB"));
    QCOMPARE(schedule->at(2)->uncode(), QString("DDDDD"));
    QCOMPARE(schedule->at(3)->uncode(), QString("CCCCC"));
    QCOMPARE(schedule->at(4)->uncode(), QString("EEEEE"));
    QCOMPARE(schedule->at(5)->uncode(), QString("FFFFF"));

    QCOMPARE(spyabout.count(), 1);
    QCOMPARE(spydone.count(), 1);
    QCOMPARE(spyabout.at(0).count(), 2);
    QCOMPARE(spyabout.at(0).at(0).toInt(), 2);
    QCOMPARE(spyabout.at(0).at(1).toInt(), 3);
    QCOMPARE(spydone.at(0).count(), 2);
    QCOMPARE(spydone.at(0).at(0).toInt(), 2);
    QCOMPARE(spydone.at(0).at(1).toInt(), 3);

    delete schedule;
}

void TestChangeRotation::testMoveUp() {
    Schedule* schedule = createSchedule();
    QSignalSpy spyabout(schedule, SIGNAL(rotationAboutToBeChanged(int, int)));
    QSignalSpy spydone(schedule, SIGNAL(rotationChanged(int, int)));
    QVERIFY(spyabout.isValid());
    QVERIFY(spydone.isValid());

    schedule->changeOfRotation(3, 2);

    QCOMPARE(schedule->at(0)->uncode(), QString("AAAAA"));
    QCOMPARE(schedule->at(1)->uncode(), QString("BBBBB"));
    QCOMPARE(schedule->at(2)->uncode(), QString("DDDDD"));
    QCOMPARE(schedule->at(3)->uncode(), QString("CCCCC"));
    QCOMPARE(schedule->at(4)->uncode(), QString("EEEEE"));
    QCOMPARE(schedule->at(5)->uncode(), QString("FFFFF"));

    QCOMPARE(spyabout.count(), 1);
    QCOMPARE(spydone.count(), 1);
    QCOMPARE(spyabout.at(0).count(), 2);
    QCOMPARE(spyabout.at(0).at(0).toInt(), 3);
    QCOMPARE(spyabout.at(0).at(1).toInt(), 2);
    QCOMPARE(spydone.at(0).count(), 2);
    QCOMPARE(spydone.at(0).at(0).toInt(), 3);
    QCOMPARE(spydone.at(0).at(1).toInt(), 2);

    delete schedule;

}

void TestChangeRotation::testMoveSame() {
    Schedule* schedule = createSchedule();
    QSignalSpy spyabout(schedule, SIGNAL(rotationAboutToBeChanged(int, int)));
    QSignalSpy spydone(schedule, SIGNAL(rotationChanged(int, int)));
    QVERIFY(spyabout.isValid());
    QVERIFY(spydone.isValid());

    schedule->changeOfRotation(2, 2);

    QCOMPARE(schedule->at(0)->uncode(), QString("AAAAA"));
    QCOMPARE(schedule->at(1)->uncode(), QString("BBBBB"));
    QCOMPARE(schedule->at(2)->uncode(), QString("CCCCC"));
    QCOMPARE(schedule->at(3)->uncode(), QString("DDDDD"));
    QCOMPARE(schedule->at(4)->uncode(), QString("EEEEE"));
    QCOMPARE(schedule->at(5)->uncode(), QString("FFFFF"));

    QCOMPARE(spyabout.count(), 1);
    QCOMPARE(spydone.count(), 1);
    QCOMPARE(spyabout.at(0).count(), 2);
    QCOMPARE(spyabout.at(0).at(0).toInt(), 2);
    QCOMPARE(spyabout.at(0).at(1).toInt(), 2);
    QCOMPARE(spydone.at(0).count(), 2);
    QCOMPARE(spydone.at(0).at(0).toInt(), 2);
    QCOMPARE(spydone.at(0).at(1).toInt(), 2);

    delete schedule;
}

void TestChangeRotation::testMoveTwoDown() {
    Schedule* schedule = createSchedule();
    QSignalSpy spyabout(schedule, SIGNAL(rotationAboutToBeChanged(int, int)));
    QSignalSpy spydone(schedule, SIGNAL(rotationChanged(int, int)));
    QVERIFY(spyabout.isValid());
    QVERIFY(spydone.isValid());

    schedule->changeOfRotation(2, 4);

    QCOMPARE(schedule->at(0)->uncode(), QString("AAAAA"));
    QCOMPARE(schedule->at(1)->uncode(), QString("BBBBB"));
    QCOMPARE(schedule->at(2)->uncode(), QString("DDDDD"));
    QCOMPARE(schedule->at(3)->uncode(), QString("EEEEE"));
    QCOMPARE(schedule->at(4)->uncode(), QString("CCCCC"));
    QCOMPARE(schedule->at(5)->uncode(), QString("FFFFF"));

    QCOMPARE(spyabout.count(), 1);
    QCOMPARE(spydone.count(), 1);
    QCOMPARE(spyabout.at(0).count(), 2);
    QCOMPARE(spyabout.at(0).at(0).toInt(), 2);
    QCOMPARE(spyabout.at(0).at(1).toInt(), 4);
    QCOMPARE(spydone.at(0).count(), 2);
    QCOMPARE(spydone.at(0).at(0).toInt(), 2);
    QCOMPARE(spydone.at(0).at(1).toInt(), 4);


    delete schedule;

}

void TestChangeRotation::testMoveTwoUp() {
    Schedule* schedule = createSchedule();
    QSignalSpy spyabout(schedule, SIGNAL(rotationAboutToBeChanged(int, int)));
    QSignalSpy spydone(schedule, SIGNAL(rotationChanged(int, int)));
    QVERIFY(spyabout.isValid());
    QVERIFY(spydone.isValid());

    schedule->changeOfRotation(4, 2);

    QCOMPARE(schedule->at(0)->uncode(), QString("AAAAA"));
    QCOMPARE(schedule->at(1)->uncode(), QString("BBBBB"));
    QCOMPARE(schedule->at(2)->uncode(), QString("EEEEE"));
    QCOMPARE(schedule->at(3)->uncode(), QString("CCCCC"));
    QCOMPARE(schedule->at(4)->uncode(), QString("DDDDD"));
    QCOMPARE(schedule->at(5)->uncode(), QString("FFFFF"));

    QCOMPARE(spyabout.count(), 1);
    QCOMPARE(spydone.count(), 1);
    QCOMPARE(spyabout.at(0).count(), 2);
    QCOMPARE(spyabout.at(0).at(0).toInt(), 4);
    QCOMPARE(spyabout.at(0).at(1).toInt(), 2);
    QCOMPARE(spydone.at(0).count(), 2);
    QCOMPARE(spydone.at(0).at(0).toInt(), 4);
    QCOMPARE(spydone.at(0).at(1).toInt(), 2);

    delete schedule;



}





QTEST_MAIN(TestChangeRotation);
#include "testchangerotation.moc"
