#include <QtTest>

#include "call.h"
#include "schedule.h"

#include <QObject>

using namespace ange::schedule;

class ScheduleTest : public QObject {
    Q_OBJECT
private Q_SLOTS:
    void testGetCallByUncode_data();
    void testGetCallByUncode();
};

QTEST_MAIN(ScheduleTest);

void ScheduleTest::testGetCallByUncode_data() {
    QTest::addColumn<int>("fromIndex");
    QTest::addColumn<int>("expectedBeforeIndex");
    QTest::addColumn<int>("expectedAfterIndex");

    QTest::newRow("0") << 0 << -1 <<  1;
    QTest::newRow("1") << 1 <<  1 <<  3;
    QTest::newRow("2") << 2 <<  1 <<  3;
    QTest::newRow("3") << 3 <<  3 << -1;
    QTest::newRow("4") << 4 <<  3 << -1;
}

/**
 * Test getCallByUncode methods
 * Checks that getCallByUncodeBeforeCall is inclusive and getCallByUncodeAfterCall is not
 */
void ScheduleTest::testGetCallByUncode() {
    QFETCH(int, fromIndex);
    QFETCH(int, expectedBeforeIndex);
    QFETCH(int, expectedAfterIndex);

    QScopedPointer<Schedule> schedule(new Schedule(QList<Call*>()));
    schedule->insert(0, new Call("AAAAA", "1"));
    schedule->insert(1, new Call("XXXXX", "2"));
    schedule->insert(2, new Call("BBBBB", "3"));
    schedule->insert(3, new Call("XXXXX", "4"));
    schedule->insert(4, new Call("CCCCC", "5"));

    const Call* before = schedule->getCallByUncodeBeforeCall("XXXXX", schedule->at(fromIndex));
    QCOMPARE(before, expectedBeforeIndex == -1 ? 0 : schedule->at(expectedBeforeIndex));

    const Call* after = schedule->getCallByUncodeAfterCall("XXXXX", schedule->at(fromIndex));
    QCOMPARE(after, expectedAfterIndex == -1 ? 0 : schedule->at(expectedAfterIndex));
}

#include "scheduletest.moc"
