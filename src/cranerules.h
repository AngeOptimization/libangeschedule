#ifndef ANGE_SCHEDULE_CRANERULES_H
#define ANGE_SCHEDULE_CRANERULES_H

#include <QString>
#include <QMetaType>

namespace ange {
namespace schedule {

struct CraneRules {

    /**
     * enums describing the possible cranerules.
     */
    enum Types {
        CraneRuleUnknown = 0,
        CraneRuleFourty = 1,
        CraneRuleEighty = 2,
        CraneRuleFourtySuperBins = 3
    };

    static QString toString(Types rule) {
        switch (rule) {
            case CraneRuleUnknown :
                return QString("");
            case CraneRuleFourty :
                return QString("40'");
            case CraneRuleEighty :
                return QString("80'");
            case CraneRuleFourtySuperBins :
                return QString("40'+");
        };
        Q_ASSERT(false);
        return QString("");
    }

};

}}  // namespace ange::schedule

Q_DECLARE_METATYPE(ange::schedule::CraneRules::Types)

#endif  // ANGE_SCHEDULE_CRANERULES_H
