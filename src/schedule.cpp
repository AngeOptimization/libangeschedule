#include "schedule.h"

#include "call.h"

#include <stdexcept>

namespace ange {
namespace schedule {

class SchedulePrivate {
public:
    SchedulePrivate(const QList<Call*>& calls) : m_calls(calls), m_pivot_call_index(-1) {}
    QList<Call*> m_calls;
    void update_rotation_no();
    int m_pivot_call_index;
    //Caching of the between functions. It is kind of expensive and can be frequently called
    typedef QPair<const ange::schedule::Call*, const ange::schedule::Call*> CallPair;
    mutable QHash<CallPair, QList<ange::schedule::Call*> > m_betweenCache;
    QList<Call*> between(const ange::schedule::Call* from, const ange::schedule::Call* to) {
        int from_index = m_calls.indexOf(const_cast<Call*>(from));
        const int to_index = m_calls.indexOf(const_cast<Call*>(to));
        if (from_index != -1 && to_index != -1 && from_index > to_index) {
            Q_ASSERT(false);
            return QList<Call*>();
        }
        QList<Call*> rv;
        for (; from_index < to_index; ++from_index) {
            rv << m_calls[from_index];
        }
        return rv;
    }
};

void SchedulePrivate::update_rotation_no() {
    int rotation_no = 0;
    Q_FOREACH (Call* call, m_calls) {
        call->setRotationNo(rotation_no++);
    }
}

Schedule::Schedule(QList< Call* > calls, QObject* parent) : QObject(parent), d(new SchedulePrivate(calls)) {
    Q_FOREACH (Call* call, calls) {
        call->setParent(this);
    }
    d->update_rotation_no();
}

Schedule::~Schedule() {
    qDeleteAll(d->m_calls);
}

const QList<Call*>& Schedule::calls() const {
    return d->m_calls;
}

int Schedule::size() const {
    return d->m_calls.size();
}

const Call* Schedule::getCallByUncodeAfterCall(const QString& uncode, const ange::schedule::Call* from) const {
    bool found = false;
    foreach (Call * call, d->m_calls) {
        if (found) {
            if (call->uncode() == uncode) {
                return call;
            }
        }
        found = found || (call == from);
    }
    return 0;
}

const ange::schedule::Call* Schedule::getCallByUncodeBeforeCall(const QString& uncode, const ange::schedule::Call* before) const {
    bool found = false;
    for (int i = d->m_calls.size() - 1; i >= 0; --i) {
        Call* call = d->m_calls.value(i);
        found = found || (call == before);
        if (found) {
            if (call->uncode() == uncode) {
                return call;
            }
        }
    }
    return 0;
}

bool Schedule::less(const Call* lhs, const Call* rhs) const {
    // WARNING this metod will return if only one of the calls is in the schedule!
    Q_ASSERT(lhs);
    Q_ASSERT(rhs);
    Q_FOREACH (const Call* call, d->m_calls) {
        if (rhs == call) {
            return false;
        }
        if (lhs == call) {
            return true;
        }
    }
    throw std::runtime_error("Attempt to compare two calls not in schedule");
}

const Call* Schedule::getCallById(int id) const {
    Q_FOREACH (const Call* call, d->m_calls) {
        if (call->id() == id) {
            return call;
        }
    }
    return 0;
}

Call* Schedule::getCallById(int id) {
    Q_FOREACH (Call* call, d->m_calls) {
        if (call->id() == id) {
            return call;
        }
    }
    return 0;
}


void Schedule::insert(int position, ange::schedule::Call* call) {
    emit callAboutToBeAdded(position, call);
    d->m_calls.insert(position, call);
    call->setParent(this);
    d->update_rotation_no();
    if (position < d->m_pivot_call_index) {
        d->m_pivot_call_index++;
    }
    d->m_betweenCache.clear();
    emit callAdded(call);
    emit scheduleChanged();
}

void Schedule::removeAt(int position) {
    Call* call = d->m_calls.at(position);
    emit callAboutToBeRemoved(call);
    if (d->m_pivot_call_index >= position) {
        d->m_pivot_call_index--;
    }
    d->m_calls.removeAt(position);
    d->update_rotation_no();
    d->m_betweenCache.clear();
    emit callRemoved(call);
    emit scheduleChanged();
    delete call;
}

void Schedule::changeOfRotation(int position1, int position2) {
    emit rotationAboutToBeChanged(position1, position2);
    Call* movedCall = d->m_calls.takeAt(position1);
    d->m_calls.insert(position2, movedCall);
    d->update_rotation_no();
    d->m_betweenCache.clear();
    emit rotationChanged(position1, position2);
    emit scheduleChanged();
}

ange::schedule::Call* Schedule::at(int i) const {
    return d->m_calls.value(i, 0);
}

int Schedule::indexOf(const ange::schedule::Call* call) const {
    if (!call) {
        return -1;
    }
    int rotation_no = call->rotationNo();
    if (d->m_calls.size() > rotation_no && d->m_calls[rotation_no]) {
        return rotation_no;
    }
    Q_ASSERT(call->parent() != this);
    return -1;
}

QList< Call* > Schedule::between(const ange::schedule::Call* from, const ange::schedule::Call* to) const {
    SchedulePrivate::CallPair p(from, to);
    QHash<SchedulePrivate::CallPair, QList<ange::schedule::Call*> >::const_iterator it = d->m_betweenCache.constFind(p);
    if (it != d->m_betweenCache.constEnd()) {
#ifndef QT_NO_DEBUG
        QList<ange::schedule::Call*> calllist = d->between(from, to);
        Q_ASSERT(calllist == it.value());
#endif
        return it.value();
    }
    QList<ange::schedule::Call*> calllist = d->between(from, to);
    d->m_betweenCache.insert(p, calllist);
    return calllist;
}

const ange::schedule::Call* Schedule::next(const ange::schedule::Call* call) const {
    return d->m_calls.value(indexOf(call) + 1, 0L);
}

Call* Schedule::next(const ange::schedule::Call* call) {
    return d->m_calls.value(indexOf(call) + 1, 0L);
}

const ange::schedule::Call* Schedule::previous(const ange::schedule::Call* call) const {
    return d->m_calls.value(indexOf(call) - 1, 0L);
}

ange::schedule::Call* Schedule::previous(const ange::schedule::Call* call) {
    return d->m_calls.value(indexOf(call) - 1, 0L);
}

const ange::schedule::Call* ange::schedule::Schedule::pivotCall() const {
    return d->m_calls.value(d->m_pivot_call_index, 0);
}

void ange::schedule::Schedule::setPivotCall(const int new_pivot_port_index) {
    d->m_pivot_call_index = new_pivot_port_index;
    emit scheduleChanged();
}

}} // namespace ange::schedule

#include "schedule.moc"
