#include "call.h"

#include "cranerules.h"
#include "schedule.h"

#include <limits>
#include <cmath>

namespace ange {
namespace schedule {

class Call::CallData {
public:
    CallData(const QString& uncode, const QString& name, const QDateTime& eta, const QDateTime& etd,
             const QString& voyage_code) :
        m_uncode(uncode),
        m_name(name.isEmpty() ? uncode : name),
        m_rotation_no(-1),
        m_eta(eta),
        m_etd(etd),
        m_voyage_code(voyage_code),
        m_cranes(std::numeric_limits<double>::quiet_NaN()),
        m_height_limit(std::numeric_limits<double>::quiet_NaN()),
        m_crane_rule(CraneRules::CraneRuleUnknown),
        m_twinlifting(TwinLiftRules::TwinliftRuleUnknown),
        m_craneProductivity(std::numeric_limits<double>::quiet_NaN())
    {
        // Empty
    }
    const QString m_uncode;
    const QString m_name;
    int m_rotation_no;
    QDateTime m_eta;
    QDateTime m_etd;
    QString m_voyage_code;
    double m_cranes;
    double m_height_limit;
    CraneRules::Types m_crane_rule;
    TwinLiftRules::Types m_twinlifting;
    double m_craneProductivity;
};

Call::Call(const QString& uncode, const QString& voyage_code, QObject* parent, int id)
  : QObject(parent), Identifiable<Call>(id),
    d(new CallData(uncode, QString(), QDateTime(), QDateTime(), voyage_code))
{
    // Empty
}

Call::Call(const QString& uncode, const QString& voyage_code, const QString& port_name, const QDateTime& eta,
           const QDateTime& etd, QObject* parent, int id)
  : QObject(parent), Identifiable<ange::schedule::Call>(id),
    d(new CallData(uncode, port_name, eta, etd, voyage_code))
{
    Q_ASSERT(!(etd < eta));
}

Call::~Call() {
    // Empty
}

const QString& Call::uncode() const {
    return d->m_uncode;
}

const QString& Call::name() const {
    return d->m_name;
}

void Call::setEta(QDateTime eta) {
    d->m_eta = eta;
    emit etaChanged(eta);
}

void Call::setEtd(QDateTime etd) {
    d->m_etd = etd;
    emit etdChanged(etd);
}

const QDateTime& Call::eta() const {
    return d->m_eta;
}

const QDateTime& Call::etd() const {
    return d->m_etd;
}

const QString& Call::voyageCode() const {
    return d->m_voyage_code;
}

int Call::distance(const Call* dest) const {
    Q_ASSERT(dest);
    Q_ASSERT(parent() == dest->parent()); // validate that the two calls are in the same schedule
    return dest->d->m_rotation_no - d->m_rotation_no;
}

bool Call::operator<(const Call& rhs) const {
    Q_ASSERT(parent() == rhs.parent());  // validate that the two calls are in the same schedule
    bool result = d->m_rotation_no < rhs.d->m_rotation_no;
    Q_ASSERT(!(*this == rhs && result));  // check that a call never is less than itself
    return result;
}

bool Call::lessThan(const Call* rhs) const {
    return *this < *rhs;
}

double Call::cranes() const {
    return d->m_cranes;
}

double Call::heightLimit() const {
    return d->m_height_limit;
}

double Call::craneProductivity() const {
    return d->m_craneProductivity;
}

void Call::setCranes(double cranes) {
    if (std::isnan(cranes) != std::isnan(d->m_cranes) || std::fabs(d->m_cranes - cranes) > 1e-10) {
        d->m_cranes = cranes;
        emit cranesChanged(cranes);
    }
}

void Call::setHeightLimit(double height_limit) {
    if (std::isnan(height_limit) != std::isnan(d->m_height_limit) || std::fabs(d->m_height_limit - height_limit) > 1e-10) {
        d->m_height_limit = height_limit;
        emit heightLimitChanged(height_limit);
    }
}

CraneRules::Types Call::craneRule() const {
    return d->m_crane_rule;
}

void Call::setCraneRule(CraneRules::Types rule) {
    d->m_crane_rule = rule;
    emit craneRuleChanged(rule);
}

void Call::setTwinlifting(TwinLiftRules::Types twinlifting) {
    d->m_twinlifting = twinlifting;
    emit twinLiftingChanged(twinlifting);
}

TwinLiftRules::Types Call::twinLifting() const {
    return d->m_twinlifting;
}

void Call::setCraneProductivity(double productivity) {
    d->m_craneProductivity = productivity;
    emit craneProductivityChanged(productivity);
}

QDebug operator<<(QDebug str, const Call& call) {
    return str << call.assembledName();
}

QDebug operator<<(QDebug str, const Call* call) {
    if (call) {
        return str << *call;
    } else {
        return str << QString("<null>");
    }
}

void Call::setRotationNo(int rotation_no) const {
    d->m_rotation_no = rotation_no; // Really a const-violation, but the private-impl pattern destroys this.
}

int Call::rotationNo() const {
    return d->m_rotation_no;
}

void Call::setVoyageCode(QString voyage_code) {
    if (voyage_code != d->m_voyage_code) {
        d->m_voyage_code = voyage_code;
        emit voyageCodeChanged(voyage_code);
    }
}

QString Call::assembledName() const {
    return QString("%1/%2").arg(d->m_uncode).arg(d->m_voyage_code);
}

} // namespace schedule
} // namespace ange
